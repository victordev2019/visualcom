<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\Quotation\QuotationController;
use App\Http\Controllers\Admin\Quotation\ServiceController;
use App\Http\Controllers\Admin\Reports\Quotation\QuotationController as QuotationQuotationController;
use Illuminate\Support\Facades\Route;

Route::get('', [HomeController::class, 'index']);
Route::controller(QuotationController::class)->group(function () {
    Route::get('quotations', 'index')->name('quotations.index');
    Route::get('quotations/{id}/price', 'price')->name('quotations.price');
    Route::get('quotations/{quotation}/edit', 'edit')->name('quotations.edit');
    Route::get('quotations/create', 'create')->name('quotations.create');
    Route::get('event/{quotation}/create', 'eventCreate')->name('event.create');
});
Route::controller(ServiceController::class)->group(function () {
    Route::get('services', 'index')->name('services.index');
});
// REPORTS ROUTES
Route::get('quotation/{id}/view', [QuotationQuotationController::class, 'index'])->name('view.quotation');
Route::get('quotation/{id}/pdf', [QuotationQuotationController::class, 'getQuotation'])->name('pdf.quotation');
