<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Victor Avalos Torrez',
            'email' => 'tigervat10@outlook.com',
            'password' => bcrypt('visual.com.vat')
        ]);
        User::create([
            'name' => 'David',
            'email' => 'davidperalt@hotmail.com',
            'password' => bcrypt('visual.com.bo')
        ]);
    }
}
