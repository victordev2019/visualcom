<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mappings = ['TOTEMS BASTIDOR CON LONA BLANCA', 'PROYECTORES MULTIMEDIA DE 20 K LUMENES TECNOLOGIA LASER', 'SERVIDOR DE VIDEO', 'ADECUACION DE CONTENIDO 2D Y 3D PARA EL MAPPING', 'CABLEADO Y ACCESORIOS'];
        $units = ['PZ', 'UN', 'MT', 'M2'];
        $prices = [750, 1200.50, 2105.99, 1000];
        foreach ($mappings as $mapping) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $mapping,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 1
            ]);
        }
        $iluminaciones = ['CABEZAS MOVILES BEAM', 'BRINDERS', 'MAQUINADE HUMO', 'CONSOLA PARA LAS LUCES', 'CABLEADO Y ACCESORIOS'];
        foreach ($iluminaciones as $iluminacion) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $iluminacion,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 2
            ]);
        }
        $tarimas = ['MODULO DE 2X1', 'TRUSS DE 3 MTS PARA ESCENARIO'];
        foreach ($tarimas as $tarima) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $tarima,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 3
            ]);
        }
        $jbls = ['UNIDADES JBL', 'CONSOLA DE 16 CANALES', 'COMPUTADORA PORTATIL', 'MICROFONOS INALAMBRICOS DE MANO', 'MICROFONO ALAMBRICO', 'PEDESTAL PARA EL MICROFONO', 'CABLEADO Y ACCESORIOS'];
        foreach ($jbls as $jbl) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $jbl,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 4
            ]);
        }
        $decorativas = ['PAR LED', 'CABLEADO Y ACCESORIOS'];
        foreach ($decorativas as $decorativa) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $decorativa,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 5
            ]);
        }
        $nexos = ['UNIDADES LINE ARRAY NEXO', 'BAJOS', 'UNIDADES JBL (MONITOREO)', 'CONSOLA DIGITAL DE 24 CH.', 'COMPUTADORA PORTATIL', 'PROCESADOR ELECTRICO M60 NEXO AMP.', 'MICROFONOS INALAMBRICOS DE MANO', 'MICROFONOS HEADSET', 'SET DE PEDESTALES PARA LOS MICROFONOS', 'SOPORTE DE ELEVACION GUIL O WOK (PARA EL SONIDO)', 'CABLEADO Y ACCESORIOS'];
        foreach ($nexos as $nexo) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $nexo,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 6
            ]);
        }
        $leds = ['50 MTS DE PANTALLA LED SMD PH 4 MM ALTA DEFINICION', 'PROCESADOR DE VIDEO CON SALIDAS DE SDI, DVI, VGA Y RCA', 'COMPUTADORA PORTATIL', 'ESTRUCTURA NECESARIA PARA PANTALLA LED', 'CABLEADO Y ACCESORIOS'];
        foreach ($leds as $led) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $led,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 7
            ]);
        }
        $equipos = ['TRANSPORTE DE IDA Y VUELTA AL LUGAR DEL EVENTO'];
        foreach ($equipos as $equipo) {
            $i = array_rand($units);
            $n = array_rand($prices);
            Service::create([
                'name' => $equipo,
                'unit' => $units[$i],
                'price' => $prices[$n],
                'type_id' => 8
            ]);
        }
    }
}
