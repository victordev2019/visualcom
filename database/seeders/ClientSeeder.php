<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        Client::create([
            'name' => 'BOLIVIA TV',
            'responsible' => $faker->name(),
            'email' => $faker->safeEmail(),
            'phone' => $faker->phoneNumber()
        ]);
        Client::create([
            'name' => 'POLICIA NACIONAL',
            'responsible' => $faker->name(),
            'email' => $faker->safeEmail(),
            'phone' => $faker->phoneNumber()
        ]);
        Client::create([
            'name' => 'TAG',
            'responsible' => $faker->name(),
            'email' => $faker->safeEmail(),
            'phone' => $faker->phoneNumber()
        ]);
    }
}
