<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['MAPPING', 'SISTEMA DE ILUMINACION', 'TARIMA', 'SISTEMA DE AUDIO JBL', 'SISTEMA DE ILUMINACION DECORATIVA', 'SISTEMA DE AUDIO NEXO', 'PANTALLA LED', 'EQUIPO REQUERIDO'];
        foreach ($types as $type) {
            Type::create([
                'name' => $type
            ]);
        }
    }
}
