@props(['for'])
@error($for)
    <div class="invalid-feedback">
        {{ $message }}
        {{-- <i class="fas fa-exclamation-triangle"></i><small class="ml-2">{{ $message }}</small> --}}
    </div>
@enderror
