<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead class="bg-info">
            <tr>
                {{ $head }}
            </tr>
        </thead>
        <tbody>
            {{ $data }}
        </tbody>
    </table>
</div>
