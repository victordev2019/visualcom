<div>
    <div class="card px-3 py-2">
        <form>
            <div class="d-flex justify-content-around mt-2 mb-3">
                <div class="flex-fill mr-2">
                    <input wire:model='search' type="text" class="form-control"
                        placeholder="Buscar por código de cotización">
                </div>
                <div class="">
                    <a class="btn btn-primary" href="{{ route('quotations.create') }}"><i
                            class="fas fa-plus mr-md-2 mr-lg-2"></i><span
                            class="d-none d-md-inline-block d-lg-inline-block">Nuevo</span></a>
                </div>
            </div>
        </form>
        <div class="">
            @if (session()->has('info'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Éxito!</strong> {{ session('info') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            {{-- <p>{{ $quotations->count() }}</p> --}}
            @if ($quotations->count())
                <x-admin.common.table>
                    <x-slot name='head'>
                        <th wire:click='order("code")' class="" role="button">Código
                            @if ($sort == 'code')
                                @if ($direction == 'asc')
                                    <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                                @else
                                    <i class="fas fa-sort-alpha-down-alt float-right mt-1"></i>
                                @endif
                            @else
                                <i class="fas fa-sort float-right mt-1"></i>
                            @endif
                        </th>
                        <th wire:click='order("date")' role="button">Fecha
                            @if ($sort == 'date')
                                @if ($direction == 'asc')
                                    <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                                @else
                                    <i class="fas fa-sort-alpha-down-alt float-right mt-1"></i>
                                @endif
                            @else
                                <i class="fas fa-sort float-right mt-1"></i>
                            @endif
                        </th>
                        <th class="">Referencia</th>
                        <th wire:click='order("status")' class="" role="button">Estado
                            @if ($sort == 'status')
                                @if ($direction == 'asc')
                                    <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                                @else
                                    <i class="fas fa-sort-alpha-down-alt float-right mt-1"></i>
                                @endif
                            @else
                                <i class="fas fa-sort float-right mt-1"></i>
                            @endif
                        </th>
                        <th>Ciente</th>
                        <th>Opciones</th>
                    </x-slot>
                    <x-slot name='data'>
                        @foreach ($quotations as $quotation)
                            <tr>
                                <td>{{ $quotation->code }}</td>
                                <td style="width: 100px">{{ Date::parse($quotation->date)->format('d/m/Y') }}</td>
                                <td>{{ $quotation->reference }}</td>
                                <td style="width: 100px">
                                    @switch($quotation->status)
                                        @case(1)
                                            <span class="badge badge-pill badge-warning">espera</span>
                                        @break

                                        @case(2)
                                            <span class="badge badge-pill badge-secondary">negociando</span>
                                        @break

                                        @case(3)
                                            <span class="badge badge-pill badge-success">en curso</span>
                                        @break

                                        @default
                                    @endswitch
                                    {{-- {{ $quotation->status }} --}}
                                </td>
                                <td>{{ $quotation->client->name }}</td>
                                <td colspan="3" style="width: 110px">
                                    <a href="{{ route('quotations.edit', $quotation) }}" class="btn btn-success"><i
                                            class="fas fa-edit"></i></a>
                                    <button wire:click="$emit('del',{{ $quotation }},'{{ $quotation->code }}')"
                                        class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                    <a href="{{ route('event.create', $quotation) }}" class="btn btn-secondary"><i
                                            class="fas fa-eye"></i></a>
                                    {{-- <button class="btn btn-outline-primary"><i class="fas fa-cogs"></i></button> --}}
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-admin.common.table>
            @else
                <div class="alert alert-warning" role="alert">
                    <i class="fas fa-exclamation-triangle mr-2"></i> No se han encontrado registros!!!
                </div>
            @endif
        </div>
    </div>
</div>
