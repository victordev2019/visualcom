<div class="container">
    <div class="d-flex align-items-center justify-content-between">
        <h4 class="text-uppercase text-black-50">tipo de servicio</h4>
        <p>
            <button class="btn btn-primary collapsed" type="button" data-toggle="collapse"
                data-target="#collapseTypeService" aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-plus"></i>
            </button>
        </p>
    </div>
    {{-- modal --}}
    <div wire:ignore.self class="collapse" id="collapseTypeService" style="">
        <div class="card card-body">
            <div class="form-row">
                <div class="form-group col-md-11">
                    {{-- <label for="inputCity">City</label> --}}
                    <input wire:model='name' type="text" class="form-control" id="inputCity"
                        placeholder="Nombre tipo de servicio">
                </div>
                {{-- {{ $name }} --}}
                <div class="form-group col-md-1">
                    {{-- <label for="inputZip">Zip</label> --}}
                    <button wire:click='store' type="button" class="btn btn-primary btn-block">Crear</button>
                </div>
                @error('name')
                    <span class="text-sm text-danger ml-2"><i
                            class="fas fa-exclamation-triangle mr-1"></i>{{ $message }}</span>
                @enderror
                {{-- <x-admin.common.error-feedback for='name' /> --}}
            </div>
        </div>
    </div>

</div>
