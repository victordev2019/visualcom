<div>
    <button class="btn btn-outline-success ml-2 btn-sm" type="button" data-toggle="modal"
        data-target="#editServiceModal{{ $service->id }}"><i class="fas fa-edit"></i></button>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="editServiceModal{{ $service->id }}" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenteredLabel">Modificar Servicio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Nombre Servicio</label>
                            <input wire:model='name' type="text" class="form-control" id="formGroupExampleInput"
                                placeholder="Nombre servicio">
                            @error('name')
                                <span class="text-danger text-xs">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Unidad/Medida</label>
                            <div class="row">
                                <div class="col-6">
                                    <input wire:model='unit' type="text" class="form-control"
                                        id="formGroupExampleInput2" placeholder="Unidad" maxlength="2">
                                </div>
                                <div class="col-6">
                                    <select wire:model='unit' class="custom-select">
                                        <option selected="">Seleccione unidad...</option>
                                        @foreach ($units as $unt)
                                            <option value="{{ $unt->unit }}">
                                                {{ $unt->unit }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @error('unit')
                                <span class="text-danger text-xs">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Precio Unidad</label>
                            <input wire:model='price' type="number" value="0" min="0" class="form-control"
                                id="formGroupExampleInput2">
                            @error('price')
                                <span class="text-danger text-xs">{{ $message }}</span>
                            @enderror
                            @if (session('msgp'))
                                <span class="text-danger" style="font-size: 11.5px">{{ session('msgp') }}</span>
                            @endif
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button wire:click='edit' type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
