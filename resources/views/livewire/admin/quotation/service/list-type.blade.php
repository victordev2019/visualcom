<div class="container">
    {{-- <h3 class="text-black-50 text-uppercase">lista de tipos</h3> --}}
    <div>
        <div class="form-group">
            <input wire:model='search' type="text" class="form-control" id="formGroupExampleInput"
                placeholder="Buscar por nombre">
        </div>
    </div>
    @if ($types->count())
        <ul class="pl-0">
            <hr class="m-0">
            @foreach ($types as $type)
                <li class="d-flex justify-content-between align-items-center">
                    <p class="blockquote text-uppercase m-2">{{ $type->name }}</p>
                    <div class="">
                        <button wire:click='$emit("del",{{ $type }},"{{ $type->name }}")'
                            class="btn btn-outline-danger ml-2 btn-sm" type="button"><i
                                class="fas fa-trash-alt"></i></button>
                        <button class="btn btn-outline-success ml-2 btn-sm" type="button" data-toggle="collapse"
                            data-target="#collapseTypeEdit{{ $type->id }}" aria-expanded="true"
                            aria-controls="collapseExample"><i class="fas fa-edit"></i></button>
                    </div>
                </li>
                <hr class="m-0">
                @livewire('admin.quotation.service.edit-type', ['type' => $type], key('edit-type' . $type->id))
            @endforeach
        </ul>
        @if ($types->hasPages())
            <div class="d-flex justify-content-center">
                {!! $types->links() !!}
            </div>
        @endif
    @else
        <div class="alert alert-info" role="alert">
            <strong>Información!</strong> No se han encontrado registros.
        </div>
    @endif
</div>
