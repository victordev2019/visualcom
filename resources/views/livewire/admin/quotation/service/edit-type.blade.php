<div wire:ignore.self class="collapse hidde" id="collapseTypeEdit{{ $type->id }}" style="">
    <div class="card card-body">
        {{-- UPDATE TYPE SERVICE --}}
        <div>
            <div class="row">
                <div class="col-8 col-md-10">
                    <input wire:model='type_name' type="text" class="form-control text-uppercase">
                </div>
                <div class="col-4 col-md-2">
                    <button wire:click='edit_type' class="btn btn-primary btn-block">Modificar</button>
                </div>
            </div>
            @error('type_name')
                <span class="text-xs text-danger">{{ $message }}</span>
            @enderror
        </div>
        {{-- STORE SERVICE --}}
        <div class="my-2 d-flex justify-content-between align-items-center">
            <h4 class="text-uppercase">Servicio</h4>
            <button type="button" class="btn btn-primary" data-toggle="modal"
                data-target="#storeServiceModal{{ $type->id }}">
                <i class="fas fa-plus"></i>
            </button>
            <!-- Modal -->
            <div wire:ignore.self class="modal fade" id="storeServiceModal{{ $type->id }}" tabindex="-1"
                role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenteredLabel">Nuevo Servicio</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4>{{ $type->name }}</h4>
                            <form>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Nombre Servicio</label>
                                    <input wire:model='name' type="text" class="form-control"
                                        id="formGroupExampleInput" placeholder="Nombre servicio">
                                    @error('name')
                                        <span class="text-danger text-xs">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Unidad/Medida</label>
                                    <div class="row">
                                        <div class="col-6">
                                            <input wire:model='unit' type="text" class="form-control"
                                                id="formGroupExampleInput2" placeholder="Unidad" maxlength="2">
                                        </div>
                                        <div class="col-6">
                                            <select wire:model='unit' class="custom-select">
                                                <option selected="">Seleccione unidad...</option>
                                                @foreach ($units as $unt)
                                                    <option value="{{ $unt->unit }}">
                                                        {{ $unt->unit }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @error('unit')
                                        <span class="text-danger text-xs">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Precio Unidad</label>
                                    <input wire:model='price' type="number" value="0" min="0"
                                        class="form-control" id="formGroupExampleInput2">
                                    @error('price')
                                        <span class="text-danger text-xs">{{ $message }}</span>
                                    @enderror
                                    @if (session('msgp'))
                                        <span class="text-danger"
                                            style="font-size: 11.5px">{{ session('msgp') }}</span>
                                    @endif
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button wire:click='store_service' type="button" class="btn btn-primary">Crear</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- FILL SERVICES --}}
        <div>
            <ul class="pl-2">
                <hr class="m-1">
                @if (session('msg'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>Éxito!</strong> {{ session('msg') }}
                    </div>
                @endif
                @foreach ($type->services()->latest()->get() as $service)
                    <li class="d-flex justify-content-between align-items-center m-0 p-0">
                        <p class="m-0 text-uppercase">{{ $service->unit }} - {{ $service->name }} -
                            {{ number_format($service->price, 2) }} bs
                        </p>
                        <div class="d-flex">
                            <button class="btn btn-outline-danger ml-2 btn-sm" type="button"><i
                                    class="fas fa-trash-alt"></i></button>
                            <!-- Button trigger modal -->
                            @livewire('admin.quotation.service.edit-service', ['service' => $service], key('edit-service-' . $service->id))
                        </div>
                    </li>
                    <hr class="m-1">
                @endforeach
            </ul>
        </div>
    </div>
</div>
