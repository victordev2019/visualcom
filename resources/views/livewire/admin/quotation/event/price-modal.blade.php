<div>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="modal"
        data-target="#priceModal{{ $eventserv_id }}">
        <i class="fas fa-dollar-sign mx-1"></i>
    </button>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="priceModal{{ $eventserv_id }}" tabindex="-1"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ $title }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Cantidad en {{ $unit }} </label>
                            <input wire:model='qty' type="number" min="1" class="form-control"
                                id="formGroupExampleInput">
                            @error('qty')
                                <span class="text-danger" style="font-size: 11.5px">{{ $message }}</span>
                            @enderror
                            @if (session('msgq'))
                                <span class="text-danger" style="font-size: 11.5px">{{ session('msgq') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Precio unitario</label>
                            <input wire:model='price' type="number" class="form-control" id="formGroupExampleInput2"
                                value="">
                            @error('price')
                                <span class="text-danger" style="font-size: 11.5px">{{ $message }}</span>
                            @enderror
                            @if (session('msgp'))
                                <span class="text-danger" style="font-size: 11.5px">{{ session('msgp') }}</span>
                            @endif
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button wire:click='update' type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
