<div class="container">
    {{-- <div class="container" style="overflow-y: scroll;"> --}}
    @foreach ($events as $event)
        <div class="card">
            <div class="card-header bg-primary">
                <div class="d-flex justify-content-around align-items-center">
                    <div class="flex-fill text-uppercase">
                        {{ $event->name }} lugar {{ $event->address }} fecha
                        {{ Date::parse($event->date)->format('l j F') }} inicio
                        {{ $event->hourinit }} fin {{ $event->hourend }}
                    </div>
                    <div>
                        {{-- <a wire:click='deleteEvent({{ $event->id }})' class="btn btn-primary" role="button"
                            aria-expanded="false">
                            <i class="fas fa-edit"></i>
                        </a> --}}
                        <a wire:click='$emit("delevent",{{ $event }},"{{ $event->name }}")'
                            class="btn btn-primary" role="button" aria-expanded="false">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        <a class="btn btn-primary" data-toggle="modal" data-target="#itemSelectModal{{ $event->id }}"
                            role="button">
                            <i class="fas fa-search"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @livewire('admin.quotation.event.list-item', ['event_id' => $event->id], key('list-item' . $event->id))
            </div>

            {{-- *********** MODAL SELECCION DE ITEMS ************* --}}
            <!-- Modal -->
            <div wire:ignore.self class="modal fade bd-example-modal-lg" id="itemSelectModal{{ $event->id }}"
                tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{ $event->name }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{-- ******** collapse acordeon ********* --}}
                            <div class="accordion" id="accordionExample{{ $event->id }}">
                                <div class="row">
                                    @for ($i = 0; $i < $types->count(); $i++)
                                        <div class="card col-12 col-md-6">
                                            <div class="card-header" id="heading{{ $types[$i]->id }}">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link btn-block text-left" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#collapse{{ $types[$i]->id }}"
                                                        aria-expanded="true"
                                                        aria-controls="collapse{{ $types[$i]->id }}">
                                                        {{ $types[$i]->name }}
                                                    </button>
                                                </h2>
                                            </div>

                                            <div id="collapse{{ $types[$i]->id }}"
                                                class="collapse @if ($i == 0) show
                                            @else hide @endif"
                                                aria-labelledby="heading{{ $types[$i]->id }}"
                                                data-parent="#accordionExample{{ $event->id }}">
                                                <div class="card-body">
                                                    @for ($n = 0; $n < $types[$i]->services->count(); $n++)
                                                        <div class="form-check">
                                                            <input wire:model.defer='service_id'
                                                                class="form-check-input" type="checkbox"
                                                                value="{{ $types[$i]->services[$n]->id }} "
                                                                id="defaultCheck{{ $types[$i]->services[$n]->id }}"
                                                                checked=false>
                                                            <label class="form-check-label" for="defaultCheck1">
                                                                {{ $types[$i]->services[$n]->name }}
                                                            </label>
                                                        </div>
                                                    @endfor
                                                </div>
                                                <div class="d-flex justify-content-end m-3">
                                                    <a href="{{ route('services.index') }}"
                                                        class="btn btn-secondary mr-2"><i class="fas fa-cog"></i></a>
                                                    <button wire:click='addItem({{ $event->id }})'
                                                        class="btn btn-primary">Agregar</button>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-secondary" href="{{ route('services.index') }}"><i
                                    class="fas fa-cog mr-1"></i>Configuración</a>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
