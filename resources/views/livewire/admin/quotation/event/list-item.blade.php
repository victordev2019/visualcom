<div class="">
    @for ($i = 0; $i < $services->count(); $i++)
        @if ($i == 0 || $services[$i]->type_id != $services[$i - 1]->type_id)
            <p class="bg-secondary p-2 m-0 rounded-top">{{ $services[$i]->type_name }}</p>
        @endif

        <div class="d-flex justify-content-between align-items-center my-1">
            <div class="d-flex align-items-center ml-2">
                <strong>{{ $services[$i]->quantity }}-{{ $services[$i]->service_unit }}</strong> -
                {{ $services[$i]->service_name }}
            </div>
            {{-- <div>
                @if ($services[$i]->quantity != 0)
                    <input class="form-control" type="number" name="" id=""
                        value="{{ $services[$i]->price }}">
                @endif
            </div> --}}
            <div class="d-flex align-items-center">
                @if ($services[$i]->quantity <= 0)
                    <span class="text-warning mr-3">Sin unidades ? <i class="fas fa-exclamation-triangle"></i></span>
                @else
                    <strong class="mr-4">pu/{{ number_format($services[$i]->price, 2) }} Bs</strong>
                    <strong class="mr-3">{{ number_format($services[$i]->price * $services[$i]->quantity, 2) }}
                        Bs</strong>
                @endif
                <button wire:click="$emit('del',{{ $services[$i]->id }},'{{ $services[$i]->service_name }}')"
                    class="btn btn-outline-secondary btn-sm mr-1"><i class="fas fa-trash-alt"></i></button>
                {{-- <button
                    wire:click.prevent='$emit("qty",{{ $services[$i]->id }},{{ $services[$i]->quantity }},"{{ $services[$i]->service_name }}")'
                    class="btn btn-outline-secondary btn-sm"><i class="fas fa-dollar-sign mx-1"></i></button> --}}
                @livewire('admin.quotation.event.price-modal', ['eventserv_id' => $services[$i]->id], key('price-modal-' . $services[$i]->id))
            </div>
        </div>
        <hr class="m-0">
        {{-- CALCULO SUBTOTAL ITEMS --}}
        @if ($i == $services->count() - 1 || $services[$i]->type_id != $services[$i + 1]->type_id)
            @php
                $subtotal = 0;
                foreach ($services as $service) {
                    if ($service->type_id == $services[$i]->type_id) {
                        $subtotal = $subtotal + $service->quantity * $service->price;
                    }
                }
            @endphp
            <div class="d-flex justify-content-end my-2">
                <p class="h6 text-uppercase font-weight-bold">
                    SUB TOTAL {{ $services[$i]->type_name }} bs {{ number_format($subtotal, 2) }}
                </p>
            </div>
        @endif
    @endfor
    <hr>
    {{-- CALCULO TOTAL EVENTO --}}
    @php
        $total = 0;
        foreach ($services as $service) {
            $total = $total + $service->quantity * $service->price;
        }
        // echo $total;
    @endphp
    <div class="d-flex justify-content-end mt-3 h6 text-uppercase font-weight-bold"><span>total evento bs
            {{ number_format($total, 2) }}</span></div>
</div>
