<div class="container">
    <div class="d-flex justify-content-between mb-4">
        <h4 class="text-uppercase">registro evento</h2>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseEventStore"
                aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-plus"></i>
            </button>
    </div>
    <div wire:ignore.self class="collapse" id="collapseEventStore">
        <div class="card">
            <div class="card-body">
                <form>
                    <form>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Descripción</label>
                            <input wire:model='name' type="text"
                                class="form-control @error('name') is-invalid @enderror" id="formGroupExampleInput"
                                placeholder="Descripción del evento">
                            <x-admin.common.error-feedback for='name' />
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="formGroupExampleInput2">Fecha</label>
                                <input wire:model='date' type="date" min="{{ $mind }}"
                                    max="{{ $maxd }}" class="form-control @error('date') is-invalid @enderror"
                                    id="formGroupExampleInput2" placeholder="Another input">
                                <x-admin.common.error-feedback for='date' />
                                @if (session('msg'))
                                    <span class="text-danger text-sm">{{ session('msg') }}</span>
                                @endif
                            </div>
                            <div class="form-group col">
                                <label for="formGroupExampleInput2">Hora inicio</label>
                                <input wire:model='hourinit' type="time" min="19:05"
                                    class="form-control @error('hourinit') is-invalid @enderror"
                                    id="formGroupExampleInput2" placeholder="Another input">
                                <x-admin.common.error-feedback for='hourinit' />
                            </div>
                            <div class="form-group col">
                                <label for="formGroupExampleInput2">Hora fin</label>
                                <input wire:model='hourend' type="time"
                                    class="form-control @error('hourend') is-invalid @enderror"
                                    id="formGroupExampleInput2" placeholder="Another input">
                                <x-admin.common.error-feedback for='hourend' />
                                @if (session('msg'))
                                    <span class="text-danger text-sm">{{ session('msg') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Dirección</label>
                            <input wire:model='address' type="text"
                                class="form-control @error('address') is-invalid @enderror" id="formGroupExampleInput2"
                                placeholder="Dirección del evento">
                            <x-admin.common.error-feedback for='address' />
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Ubicación (map)</label>
                            <input wire:model='location' type="text" class="form-control" id="formGroupExampleInput2"
                                placeholder="url google maps">
                        </div>
                    </form>

                </form>
            </div>
            <div class="card-footer">
                <button wire:loading.attr='disabled' wire:target='store' wire:click='store'
                    class="btn btn-primary float-right">Crear evento</button>
            </div>
        </div>
    </div>
    {{-- TABLA TEMPORAL --}}
</div>
