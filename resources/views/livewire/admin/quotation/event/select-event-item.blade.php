<div>
    @if (session()->has('info'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Éxito!</strong> {{ session('info') }}
        </div>
    @endif
    <div class="collapse p-0" id="collapseItem{{ $event->id }}">
        <div class="accordion" id="accordionExample">
            <div class="row">
                @for ($i = 0; $i < $types->count(); $i++)
                    <div class="card col-12 col-md-6">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                    data-target="#collapseType{{ $types[$i]->id }}" aria-expanded="true"
                                    aria-controls="collapseType{{ $types[$i]->id }}">
                                    {{ $types[$i]->name }}
                                </button>
                            </h2>
                        </div>
                        <div id="collapseType{{ $types[$i]->id }}"
                            class="collapse @if ($i == 0) show
                        @else
                        hide @endif"
                            aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                @for ($n = 0; $n < $types[$i]->services->count(); $n++)
                                    <div class="form-check">
                                        <input wire:model.defer='service_id' class="form-check-input" type="checkbox"
                                            value="{{ $types[$i]->services[$n]->id }} " id="defaultCheck1" checked>
                                        <label class="form-check-label" for="defaultCheck1">
                                            {{ $types[$i]->services[$n]->name }}
                                        </label>
                                    </div>
                                @endfor
                            </div>
                            <div class="d-flex justify-content-end m-3">
                                <button class="btn btn-secondary mr-2"><i class="fas fa-cog"></i></button>
                                <button wire:click='addItem({{ $event->id }})'
                                    class="btn btn-secondary">Agregar</button>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
        {{-- <div class="d-flex justify-content-end">
            <button class="btn btn-secondary mr-2"><i class="fas fa-cog"></i></button>
            <button wire:click='addItem({{ $event->id }})' class="btn btn-secondary">Agregar</button>
        </div> --}}
    </div>
</div>
