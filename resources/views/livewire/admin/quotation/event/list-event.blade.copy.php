<div class="container">
    @foreach ($events as $event)
        <div class="card">
            <div class="card-header bg-dark">
                <div class="d-flex justify-content-around align-items-center">
                    <div class="flex-fill text-uppercase">
                        {{ $event->name }} lugar {{ $event->address }} fecha
                        {{ Date::parse($event->date)->format('l j F') }} inicio
                        {{ $event->hourinit }} fin {{ $event->hourend }}
                    </div>
                    <div>
                        <a wire:click='deleteEvent({{ $event->id }})' class="btn btn-dark" role="button"
                            aria-expanded="false">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a wire:click='$emit("delevent",{{ $event }},"{{ $event->name }}")' class="btn btn-dark"
                            role="button" aria-expanded="false">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        <a class="btn btn-dark" data-toggle="collapse" href="#collapseItem{{ $event->id }}"
                            role="button" aria-expanded="false" aria-controls="collapseItem{{ $event->id }}">
                            <i class="fas fa-search"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @livewire('admin.quotation.event.select-event-item', ['event' => $event], key($event->id))
                @livewire('admin.quotation.event.list-item', ['event_id' => $event->id], key($event->id))
            </div>
        </div>
    @endforeach
</div>
