<div class="container">
    {{-- información primaria --}}
    <div class="card p-4">
        <form>
            <div class="row">
                <div class="form-group col">
                    <label for="exampleInputEmail1">Fecha</label>
                    <input wire:model='date' type="date" class="form-control is-valid" min="{{ $mind }}"
                        max="{{ $maxd }}">
                </div>
                <div class="form-group col">
                    <label for="exampleInputEmail1">Código</label>
                    <input wire:model='code' type="text" class="form-control is-valid" id="exampleInputEmail1"
                        aria-describedby="emailHelp" disabled>
                </div>
            </div>
            <div class="d-flex justify-content-around">
                <div class="form-group flex-fill mr-2">
                    <label for="exampleFormControlSelect1">Cliente</label>
                    <select wire:model='client_id' class="form-control @error('client_id') is-invalid @enderror"
                        id="exampleFormControlSelect1">
                        <option value="" selected>Selecione un cliente...</option>
                        @foreach ($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                        @endforeach
                    </select>
                    {{-- <x-admin.common.error-feedback for='client_id' /> --}}
                </div>
                <div class="form-group d-flex justify-content-end align-items-end">
                    <!-- Button trigger modal -->
                    <button wire:click.prevent='clearModal' type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#storeClientModal" disabled><i class="fas fa-plus"></i></button>
                </div>
            </div>
            <div>
                @if (session()->has('info'))
                    <div class="alert alert-success w-full" role="alert">
                        <strong>Exito!</strong> {{ session('info') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Referencia</label>
                <input wire:model='reference' type="text"
                    class="form-control @error('reference') is-invalid @enderror" id="validationText"
                    placeholder="Referencia de la cotización" required>
                <x-admin.common.error-feedback for='reference' />
            </div>
        </form>
    </div>
    {{-- estado de la cotización --}}
    <div class="card">
        <div class="card-body">
            <fieldset>
                <legend>Seleccione un estado de cotización:</legend>
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
                    <div class="col">
                        <input wire:model='status' type="radio" value="1" checked>
                        <label class="ml-1" for="huey">Espera</label>
                    </div>
                    <div class="col">
                        <input wire:model='status' type="radio" value="2">
                        <label class="ml-1" for="dewey">Negociación</label>
                    </div>

                    <div class="col">
                        <input wire:model='status' type="radio" value="3">
                        <label class="ml-1" for="dewey">En curso</label>
                    </div>
                    <div class="col">
                        <input wire:model='status' type="radio" value="4" disabled>
                        <label class="ml-1" for="dewey">Archivar</label>
                    </div>
                </div>

            </fieldset>
        </div>
    </div>
    {{-- controls --}}
    <div class="card">
        <div class="card-body">
            <div class="form-group">
                <div class="float-right">
                    <a href="{{ route('quotations.index') }}" class="btn btn-danger mr-3">Cancelar</a>
                    <button wire:click.prevent='save' class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div wire:ignore.self class="modal" id="storeClientModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenteredLabel">Nuevo Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form wire:submit.prevent='storeClientData'>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Nombre</label>
                            <input wire:model='name' type="text"
                                class="form-control @error('name') is-invalid @enderror" id="name"
                                placeholder="Nombre cliente o empresa">
                            @error('name')
                                <span class="text-danger" style="font-size: 11.5px">{{ $message }}</span>
                            @enderror
                            {{-- <x-admin.common.error-feedback for='name' /> --}}
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Reponsable</label>
                            <input wire:model='responsible' type="text"
                                class="form-control @error('name') is-invalid @enderror" id="responsible"
                                placeholder="Nombre responsable o representante">
                            @error('responsible')
                                <span class="text-danger" style="font-size: 11.5px">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Email</label>
                            <input wire:model='email' type="email"
                                class="form-control @error('name') is-invalid @enderror" id="email"
                                placeholder="Nombre responsable o representante">
                            @error('email')
                                <span class="text-danger" style="font-size: 11.5px">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Teléfono</label>
                            <input wire:model='phone' type="text"
                                class="form-control @error('phone') is-invalid @enderror" id="phone"
                                placeholder="Nombre responsable o representante">
                            @error('phone')
                                <span class="text-danger" style="font-size: 11.5px">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
