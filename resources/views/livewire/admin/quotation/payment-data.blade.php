<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between text-lg">
                {{-- <p>{{ $price }}</p> --}}
                <p class="text-uppercase"><span class="font-weight-bold">precio total: </span>{{ $pricel }}</p>
                <p class="text-uppercase font-weight-bold">{{ number_format($price, 2) }} Bs</p>
            </div>
            <div class="d-flex">
                <p class="text-uppercase"><span class="font-weight-bold">forma de pago: </span>{{ $payment }}
                </p>
                <button class="btn btn-primary ml-auto" type="button" data-toggle="collapse"
                    data-target="#collapsePayment" aria-expanded="true" aria-controls="collapseExample">
                    <i class="fas fa-money-check-alt"></i>
                </button>
            </div>
            <div class="collapse hidde" id="collapsePayment" style="">
                <div class="card card-body">
                    <form class="form-inline">
                        <label class="sr-only" for="inlineFormInputName2">Forma de pago</label>
                        <input wire:model.defer='payment' type="text" class="form-control mb-2 mr-sm-2"
                            id="inlineFormInputName2" placeholder="Forma de pago">
                        <button wire:click='payment_data' type="button"
                            class="btn btn-primary mb-2">Actualizar</button>
                    </form>
                </div>
            </div>

            {{-- <button wire:click="$emit('addPrice',{{ $quotation }})" class="btn btn-primary"><i
                    class="fas fa-money-check-alt mr-2"></i>Precio</button> --}}
        </div>
    </div>
    {{-- The whole world belongs to you. --}}
</div>
