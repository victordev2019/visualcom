<div class="mb-0">
    @for ($i = 0; $i < $services->count(); $i++)
        @if ($i == 0 || $services[$i]->type_id != $services[$i - 1]->type_id)
            <p class="bg-dark-sm px-4 py-1 text-lg font-weight-bold mt-0 mb-1 text-uppercase">
                {{ $services[$i]->type_name }}</p>
        @endif
        <div class="">
            @if ($services[$i]->quantity > 0)
                <div class="item-detail">
                    <p class="text-lg text-uppercase">
                        <strong class="">{{ $services[$i]->quantity }}-{{ $services[$i]->service_unit }}
                        </strong>{{ $services[$i]->service_name }}
                    </p>
                    <span class="text-lg">
                        <strong>{{ number_format($services[$i]->price * $services[$i]->quantity, 2) }}</strong>
                    </span>
                </div>
            @endif
            {{-- CALCULO SUBTOTAL ITEMS --}}
            @if ($i == $services->count() - 1 || $services[$i]->type_id != $services[$i + 1]->type_id)
                @php
                    $subtotal = 0;
                    foreach ($services as $service) {
                        if ($service->type_id == $services[$i]->type_id) {
                            $subtotal = $subtotal + $service->quantity * $service->price;
                        }
                    }
                @endphp
                {{-- <hr class="my-1"> --}}
                <div class="sub-total mb-1">
                    <p class="text-lg text-uppercase font-weight-bold">
                        SUB TOTAL {{ $services[$i]->type_name }} bs {{ number_format($subtotal, 2) }}
                    </p>
                </div>
            @endif
        </div>
    @endfor
    <hr class="mt-2 mb-2">
    {{-- CALCULO TOTAL EVENTO --}}
    @php
        $total = 0;
        foreach ($services as $service) {
            $total = $total + $service->quantity * $service->price;
        }
        // echo $total;
    @endphp
    <div class="total-evento">
        <p class="h6 text-uppercase font-weight-bold">total evento bs {{ number_format($total, 2) }}</p>
    </div>
</div>
