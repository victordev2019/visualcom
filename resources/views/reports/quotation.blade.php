<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cotización | {{ $quo->code }}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!-- CSS only -->
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous"> --}}
    <style>
        body {
            /* background-color: sandybrown; */
        }

        .bg-dark-md {
            background-color: #bdbdbd;
        }

        .bg-dark-sm {
            background-color: #d3d3d3;
        }

        .text-sm {
            font-size: 12px;
        }

        .text-md {
            font-size: 18px;
        }

        .text-lg {
            font-size: 22px;
        }

        .grid {
            /* background-color: sandybrown; */
            padding: 20px 1rem 10px 1rem;
            margin: .8rem 0;
        }

        .caja1 {
            /* background-color: red; */
            width: 79%;
            display: inline-block;
            margin: 0;
            padding: 0;
        }

        .caja2 {
            /* background-color: purple; */
            width: 20%;
            margin: 0;
            display: inline-block;
            text-align: right;
            padding: 0;
        }

        /* ****************** HEADER INFORME **************** */
        .head-report {
            /* background-color: salmon; */
        }

        .head-report .code-content {
            /* background-color: aquamarine; */
            position: relative;
            width: 100%;
            height: 130px;
        }

        .head-report .code-content .code-info {
            /* background-color: brown; */
            position: absolute;
            height: 100%;
            /* position: relative; */
        }

        .head-report .code-content .code-info .client {
            /* background-color: brown; */
            display: inline-block;
            margin-top: 45px;
        }

        .head-report .code-content .code {
            /* background-color: burlywood; */
            position: absolute;
            float: right;
            height: 100%;
            padding: 0;
            /* position: relative; */
        }

        .head-report .code-content .code p {
            /* background-color: goldenrod; */
            font-size: 20px;
            font-weight: bold;
        }

        /******************** DETALLE DEL INFORME **************** */
        /* ITEMS DEL INFORME */
        .item-detail {
            position: relative;
            width: 100%;
            height: 36px;
            /* background: salmon; */
        }

        .item-detail p {
            position: absolute;
            /* background: blueviolet; */
            display: inline;
            margin-left: 1rem;
        }

        .item-detail span {
            position: absolute;
            /* background: blueviolet; */
            float: right;
            margin-right: 1rem;
        }

        /* CALCULOS */
        .sub-total {
            position: relative;
            width: 100%;
            height: 35px;
            /* background-color: sandybrown; */
            /* display: block; */
        }

        .sub-total>p {
            position: absolute;
            float: right;
            margin-right: 1rem;
        }

        .total-evento {
            position: relative;
            width: 100%;
            height: 35px;
            /* background-color: sandybrown; */
            /* display: block; */
        }

        .total-evento>p {
            position: absolute;
            float: right;
            margin-right: 1rem;
        }
    </style>
</head>

<body>
    <div class="">
        {{-- header --}}
        <div class="head-report mb-4">
            <div class="code-content">
                <div class="code-info">
                    <p class="m-0">La Paz, {{ Date::parse($quo->date)->locale('es')->format('l j F Y') }}</p>
                    <p class="client"><span class="font-weight-bold">SEÑORES: </span>{{ $quo->client->name }}</p>
                </div>
                <div class="code">
                    <div class="visible-print text-center">
                        <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate(Request::root() . '/admin/event/' . $quo->code . '/create')) }}"
                            alt="" srcset="">
                        {{-- <p>Scan me to return to the original page.</p> --}}
                    </div>
                    <p class="">{{ $quo->code }}</p>
                </div>
            </div>
            <p class=""><span class="font-weight-bold">REF.- </span> {{ $quo->reference }}</p>
        </div>
        {{-- body --}}
        <div>
            @foreach ($events as $event)
                <div class="card mb-3">
                    <div class="card-header bg-dark-md py-2 px-3 rounded-top">
                        <div class="d-flex justify-content-around align-items-center">
                            <div class="flex-fill text-uppercase text-lg font-weight-bold">
                                {{ $event->name }} lugar {{ $event->address }} fecha
                                {{ Date::parse($event->date)->format('l j F') }} inicio
                                {{ $event->hourinit }} fin {{ $event->hourend }}
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0 pb-1">
                        {{-- @livewire('admin.quotation.event.select-event-item', ['event' => $event], key($event->id)) --}}
                        @livewire('reports.quotation.event.list-item', ['event_id' => $event->id], key($event->id))
                    </div>
                </div>
            @endforeach
        </div>
        <div>
            <div class="grid bg-dark text-white rounded">
                <p class="caja1">Precio Total: <span class="text-lg">{{ $price }}</span>
                </p>
                {{-- <p class="caja2">{{ number_format($quo->price, 2) }} Bs</p> --}}
                <p class="caja2">{{ number_format($total, 2) }} Bs</p>
            </div>
            <p class="m-0">Nota: La instalación de los equipos se realizará previa coordinación con el cliente.</p>
            <p class="m-0 text-uppercase"><span class="font-weight-bold">Forma de pago: </span> {{ $quo->payment }}</p>
            {{-- <div class="visible-print text-center">
                <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate(Request::root() . '/admin/event/' . $quo->id . '/create')) }}"
                    alt="" srcset="">
            </div> --}}
            <div class="visible-print text-center">
                <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate('https://wa.me/59171935823')) }}"
                    alt="" srcset="">
                {{-- <p>Contatanos</p> --}}
            </div>
            {{-- <p>{{ Request::root() }}</p> --}}
        </div>
    </div>
</body>

</html>
