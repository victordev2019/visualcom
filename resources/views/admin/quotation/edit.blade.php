@extends('adminlte::page')

@section('title', '+ cotización')

@section('content_header')
    <div class="container">
        <h1 class="text-uppercase">EDICIÓN DE COTIZACIÓN</h1>
        {{-- <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0"
                aria-valuemax="100">50%</div>
        </div> --}}
    </div>
@stop

@section('content')
    {{-- @livewire('admin.quotation.create-quotation') --}}
    @livewire('admin.quotation.edit-quotation', ['quotation' => $quotation], key($quitation->id))
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        window.addEventListener('close-modal', event => {
            $('#storeClientModal').modal('hide');
            // console.log('listener ok :)');
        })
    </script>
@stop
