@extends('adminlte::page')

@section('title', 'Cotizaciones')

@section('content_header')
    {{-- <div class="container">
        <h1>REGISTRO DE EVENTOS</h1>
    </div> --}}
    <div class="container">
        <div class="text-secondary font-weight-bold">
            <div class="row">
                <div class="col">
                    <p class="">La Paz, {{ Date::parse($quotation->date)->locale('es')->format('l j F Y') }}</p>
                </div>
                <div class="col">
                    <p class="d-flex justify-content-end">{{ $quotation->code }}</p>
                </div>
            </div>
            <p>SEÑORES: {{ $quotation->client->name }}</p>
            <p class="">REF.- {{ $quotation->reference }}</p>
        </div>
    </div>
@stop

@section('content')
    @livewire('admin.quotation.event.create-event', ['quotation' => $quotation], key('create-event' . $quotation->id))
    {{-- <p>{{ $quotation->events }}</p> --}}
    @livewire('admin.quotation.event.list-event', ['quotation' => $quotation], key('list-event' . $quotation->id))
    {{-- @livewire('admin.quotation.event.list-event', ['quotation' => $quotation]) --}}
    {{-- price --}}
    @livewire('admin.quotation.payment-data', ['quotation' => $quotation], key('payment-data' . $quotation->id))
    {{-- <div class="container">
        <input class="form-control" value="{{ $quotation->price }}" type="text" name="" id="" disabled>
    </div> --}}
    {{-- <div class="container">
        <form action="{{ route('quotations.price', $quotation->id) }}" class="form-inline">
            <label class="sr-only" for="inlineFormInputName2">Precio</label>
            <input type="text" name="price" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2">
            <button type="submit" class="btn btn-primary mb-2">Precio</button>
        </form>
    </div> --}}

    <div class="container py-4">
        <a href="{{ route('pdf.quotation', $quotation) }}" target="_blank" class="btn btn-primary"><i
                class="fas fa-print mr-2"></i>Imprimir</a>
        {{-- <a href="{{ route('view.quotation', $quotation->id) }}" target="_blank" class="btn btn-primary"><i
                class="fas fa-download mr-2"></i>View</a> --}}

        {{-- <p>{{ Request::url() }}</p> --}}
        <div class="visible-print text-center">
            {!! QrCode::format('svg')->size(100)->color(255, 0, 0)->generate(Request::url()) !!}
            {{-- <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->generate(Request::url())) }}"
                alt="" srcset=""> --}}
            {{-- <img src="{!! $message->embedData(QrCode::format('png')->generate('Embed me into an e-mail!'), 'QrCode.png', 'image/png') !!}"> --}}
        </div>
    </div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        console.log('hi vat!!!');
        // delete item
        Livewire.on('del', (id, msg) => {
            Swal.fire({
                title: 'Esta segut@ de eliminar?',
                text: msg,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar esto!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('admin.quotation.event.list-item', 'delete', id)
                    Swal.fire(
                        'Eliminado!',
                        msg,
                        'success'
                    )
                }
            })
        })
        // delete event
        Livewire.on('delevent', (event, msg) => {
            Swal.fire({
                title: 'Esta segut@ de eliminar?',
                text: msg,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar esto!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('admin.quotation.event.list-event', 'delete', event)
                    Swal.fire(
                        'Eliminado!',
                        msg,
                        'success'
                    )
                }
            })
        })
        // agregar precio
        Livewire.on('addPrice', async (quo) => {
            const inputValue = '';

            const {
                value: price
            } = await Swal.fire({
                title: 'PPRECIO TOTAL',
                input: 'number',
                inputLabel: 'Precio',
                inputValue: inputValue,
                inputPlaceholder: '11450.99',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'El precio total es requerido!'
                    }
                }
            })

            if (price) {
                console.log(price);
                Livewire.emitTo('admin.quotation.payment-data', 'Price', quo, price);
                Swal.fire(`El precio es ${price}`);
            }
        });
        // add item quantity/
        Livewire.on('qty', async (id, q, msg) => {
            const inputValue = q;
            const {
                value: valor
            } = await Swal.fire({
                title: msg,
                input: 'number',
                inputLabel: 'Cantidad requerida',
                inputValue: inputValue,
                inputPlaceholder: '10',
                showCancelButton: true,
                inputAttributes: {
                    max: 100,
                    min: 0,
                    step: 1
                },
                inputValidator: (value) => {
                    if (!value) {
                        return 'La cantidad es requerida!'
                    } else if (value < 0) {
                        return 'La cantidad no puede ser menor ad cero!'
                    }
                }
            })

            if (valor) {
                Livewire.emitTo('admin.quotation.event.list-item', 'quantity', id, valor, p);
                Swal.fire(`${valor} - ${msg}`);
            }
        });
        // eventos collapse
        window.addEventListener('close-collapse', event => {
            $('#collapseEventStore').collapse('hide')
        })
        window.addEventListener('close-item-collapse', (id) => {
            $('#collapseItem' + id).collapse('hide')
        })
        window.addEventListener('close-modal-item', event => {
            // alert(event.detail.id);
            // $('.modal-backdrop').remove();
            $('#itemSelectModal' + event.detail.id).modal('hide')
            // $('#collapseItem' + id).collapse('hide')
        })
        window.addEventListener('price-updated', event => {
            $('#priceModal' + event.detail.id).modal('hide')
        })
    </script>
@stop
