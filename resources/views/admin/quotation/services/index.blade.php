@extends('adminlte::page')

@section('title', 'Cotizaciones')

@section('content_header')
    {{-- <div class="container">
        <h1>REGISTRO DE EVENTOS</h1>
    </div> --}}
    <div class="container">
        <h2 class="text-uppercase">Configuración de servicios</h2>
    </div>
@stop

@section('content')
    @livewire('admin.quotation.service.create-type')
    @livewire('admin.quotation.service.list-type')
    <div class="container">
        <div class="d-flex justify-content-end my-3">
            <a class="btn btn-primary mr-2" href="{{ url()->previous() }}">Hecho</a>
            <a class="btn btn-danger" href="{{ url()->previous() }}">Cancelar</a>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/css/custom.css') }}">
@stop

@section('js')
    <script>
        console.log('hi vat!!!');
        // delete type
        Livewire.on('del', (type, msg) => {
            Swal.fire({
                title: 'Esta segut@ de eliminar?',
                text: msg,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar esto!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('admin.quotation.service.list-type', 'destroy', type)
                    Swal.fire(
                        'Eliminado!',
                        msg,
                        'success'
                    )
                }
            })
        })
        // delete event
        Livewire.on('delevent', (event, msg) => {
            Swal.fire({
                title: 'Esta segut@ de eliminar?',
                text: msg,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar esto!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('admin.quotation.event.list-event', 'delete', event)
                    Swal.fire(
                        'Eliminado!',
                        msg,
                        'success'
                    )
                }
            })
        })
        // agregar precio
        Livewire.on('addPrice', async (quo) => {
            const inputValue = '';

            const {
                value: price
            } = await Swal.fire({
                title: 'PPRECIO TOTAL',
                input: 'number',
                inputLabel: 'Precio',
                inputValue: inputValue,
                inputPlaceholder: '11450.99',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'El precio total es requerido!'
                    }
                }
            })

            if (price) {
                console.log(price);
                Livewire.emitTo('admin.quotation.payment-data', 'Price', quo, price);
                Swal.fire(`El precio es ${price}`);
            }
        });
        // add item quantity/
        Livewire.on('qty', async (id, q, msg) => {
            const inputValue = q;
            const {
                value: valor
            } = await Swal.fire({
                title: msg,
                input: 'number',
                inputLabel: 'Cantidad requerida',
                inputValue: inputValue,
                inputPlaceholder: '10',
                showCancelButton: true,
                inputAttributes: {
                    max: 100,
                    min: 0,
                    step: 1
                },
                inputValidator: (value) => {
                    if (!value) {
                        return 'La cantidad es requerida!'
                    } else if (value < 0) {
                        return 'La cantidad no puede ser menor ad cero!'
                    }
                }
            })

            if (valor) {
                Livewire.emitTo('admin.quotation.event.list-item', 'quantity', id, valor, p);
                Swal.fire(`${valor} - ${msg}`);
            }
        });
        // eventos collapse
        window.addEventListener('close-collapse-type', event => {
            $('#collapseTypeService').collapse('hide')
        })
        window.addEventListener('close-item-collapse', (id) => {
            $('#collapseItem' + id).collapse('hide')
        })
        window.addEventListener('close-modal-item', event => {
            // alert(event.detail.id);
            // $('.modal-backdrop').remove();
            $('#itemSelectModal' + event.detail.id).modal('hide')
            // $('#collapseItem' + id).collapse('hide')
        })
        // MODAL STORE SERVICE
        window.addEventListener('close-store-service', event => {
            $('#storeServiceModal' + event.detail.id).modal('hide')
        })
        window.addEventListener('close-edit-service', event => {
            $('#editServiceModal' + event.detail.id).modal('hide')
        })
        // DIALOGO DE CONFIRMACIÓN
        window.addEventListener('success-dialog', event => {
            Swal.fire(
                'Éxito!',
                event.detail.msg,
                'success'
            )
        })
    </script>
@stop
