@extends('adminlte::page')

@section('title', 'Cotizaciones')

@section('content_header')
    <h1>COTIZACIONES</h1>
@stop

@section('content')
    @livewire('admin.quotation.list-quotation')
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        Livewire.on('del', (quo, code) => {
            Swal.fire({
                title: 'Esta segut@ de eliminar?',
                text: `LA COTIZACIÓN ${code}`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar esto!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('admin.quotation.list-quotation', 'destroy', quo)
                    Swal.fire(
                        'Eliminado!',
                        code,
                        'success'
                    )
                }
            })
        })
        console.log('Hi!');
    </script>
@stop
