<?php

namespace App\Http\Livewire\Reports\Quotation\Event;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ListItem extends Component
{
    public $event_id;
    public function render()
    {
        $services = DB::table('event_service')
            ->select('event_service.id', 'event_service.service_id', 'event_service.quantity', 'event_service.price', 'services.name as service_name', 'services.unit as service_unit', 'services.price as service_price', 'services.type_id', 'types.name as type_name')
            ->join('services', 'services.id', '=', 'event_service.service_id')
            ->join('types', 'types.id', '=', 'services.type_id')
            ->orderBy('types.name')
            ->orderBy('services.name')
            ->where('event_service.event_id', $this->event_id)->get();

        return view('livewire.reports.quotation.event.list-item', compact('services'));
    }
}
