<?php

namespace App\Http\Livewire\Admin\Quotation\Event;

use App\Models\Event;
use App\Models\EventService;
use App\Models\Service;
use App\Models\Type;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class ListItem extends Component
{
    protected $listeners = ['list-item-render' => 'render', 'quantity', 'delete'];
    // protected $listeners = ['quantity'];
    public $event_id;
    public $qty = 0;
    public $price;
    // public function mount($event)
    // {
    //     $this->event = $event;
    // }
    public function render()
    {
        $services = DB::table('event_service')
            ->select('event_service.id', 'event_service.service_id', 'event_service.quantity', 'event_service.price', 'services.name as service_name', 'services.unit as service_unit', 'services.price as service_price', 'services.type_id', 'types.name as type_name')
            ->join('services', 'services.id', '=', 'event_service.service_id')
            ->join('types', 'types.id', '=', 'services.type_id')
            ->orderBy('types.name')
            ->orderBy('services.name')
            ->where('event_service.event_id', $this->event_id)->get();
        // dd($services);
        return view('livewire.admin.quotation.event.list-item', compact('services'));
    }
    public function quantity()
    {
        $services = DB::table('event_service')
            ->select('event_service.id', 'event_service.service_id', 'event_service.quantity', 'event_service.price', 'services.name as service_name', 'services.unit as service_unit', 'services.price as service_price', 'services.type_id', 'types.name as type_name')
            ->join('services', 'services.id', '=', 'event_service.service_id')
            ->join('types', 'types.id', '=', 'services.type_id')
            ->orderBy('types.name')
            ->orderBy('services.name')
            ->where('event_service.event_id', $this->event_id)->get();
        // dd($services);
        return view('livewire.admin.quotation.event.list-item', compact('services'));
    }
    public function delete($id)
    {
        DB::table('event_service')->where('id', $id)->delete();
        $this->emit('render');
    }
}
