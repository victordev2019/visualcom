<?php

namespace App\Http\Livewire\Admin\Quotation\Event;

use Livewire\Component;
use App\Models\Type;
use Illuminate\Http\Request;

class SelectEventItem extends Component
{
    public $event;
    public $service_id = [];
    public $qty = [];
    public function render()
    {
        $types = Type::orderBy('name', 'asc')->get();
        return view('livewire.admin.quotation.event.select-event-item', compact('types'));
    }
    public function addItem($id)
    {
        // return dd($this->qty);
        // return dd($this->service_id);
        // $this->services = [17 => ['quantity' => 5]];
        // dd('Item adding...' . $this->event->name);
        $this->event->services()->syncWithoutDetaching($this->service_id);
        session()->flash('info', 'Los items de servicios para el evento se han agregado con éxito.');
        $this->dispatchBrowserEvent('close-item-collapse', $id);
        $this->emit('list-item-render');
        // return redirect()->route('event.create', $this->event->quotation->id);
    }
}
