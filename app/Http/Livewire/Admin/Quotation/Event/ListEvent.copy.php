<?php

namespace App\Http\Livewire\Admin\Quotation\Event;

use App\Models\Event;
use App\Models\Type;
use Livewire\Component;

class ListEvent extends Component
{
    protected $listeners = ['list-event-render' => 'render', 'deleteEvent'];
    public $quotation;
    public $events = [];
    public function mount()
    {
        // $this->events = Event::where('quotation_id', $this->quotation_id)
        //     ->orderBy('id', 'desc')
        //     ->get();
    }
    public function render()
    {
        // $this->events = Event::where('quotation_id', $this->quotation_id)
        //     ->orderBy('id', 'desc')
        //     ->get();
        $this->events = $this->quotation->events;
        return view('livewire.admin.quotation.event.list-event');
    }
    public function deleteEvent(Event $event)
    {
        // return dd('Eliminando!!!');
        $event->delete();
        // $this->events = Event::where('quotation_id', $this->quotation_id)
        //     ->orderBy('id', 'desc')
        //     ->get();
        $this->emit('render');
    }
    public function listen()
    {
        $events = Event::where('quotation_id', $this->quotation_id)
            ->orderBy('id', 'desc')
            ->get();
        // dd('renderizado!!!');
        return view('livewire.admin.quotation.event.list-event', compact('events'));
    }
}
