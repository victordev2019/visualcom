<?php

namespace App\Http\Livewire\Admin\Quotation\Event;

use App\Models\Event;
use Livewire\Component;

class CreateEvent extends Component
{
    public $quotation;
    public $name, $date, $hourinit, $hourend, $address, $location;
    public $maxd, $mind;
    protected $rules = [
        'name' => 'required',
        'date' => 'required',
        'hourinit' => 'required',
        'hourend' => 'required',
        'address' => 'required',
    ];
    public function mount()
    {
        $this->initDate();
    }
    public function render()
    {
        return view('livewire.admin.quotation.event.create-event');
    }
    public function initDate()
    {
        $this->date = $this->quotation->date;
        $this->mind = date('Y') . '-' . date('m', strtotime($this->quotation->date)) . '-' . date('d', strtotime($this->quotation->date));
        $this->maxd = date('Y') + 1 . '-12-31';
    }
    public function store()
    {
        if ($this->quotation->date > $this->date) {
            return session()->flash('msg', 'La fecha del evento es menor que la solicitud de cotizacion');
        }
        // if ($this->hourinit > $this->hourend) {
        //     return session()->flash('msg', 'La hora inicial no puede ser mayor a hora final');
        // }
        $this->validate($this->rules);
        $event = new Event();
        $event->name = $this->name;
        $event->date = $this->date;
        $event->hourinit = $this->hourinit;
        $event->hourend = $this->hourend;
        $event->address = $this->address;
        $event->location = $this->location;
        $event->quotation_id = $this->quotation->id;
        $event->save();

        $this->reset(['name', 'hourinit', 'hourend', 'address', 'location']);
        $this->initDate();

        $this->emit('list-item-render');
        $this->emit('updateEvent');
        $this->dispatchBrowserEvent('close-collapse');
        // return redirect()->route('event.create', $this->quotation->id);
    }
}
