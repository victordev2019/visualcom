<?php

namespace App\Http\Livewire\Admin\Quotation\Event;

use App\Models\Event;
use App\Models\Type;
use Livewire\Component;

class ListEvent extends Component
{
    protected $listeners = ['updateEvent' => 'render', 'delete'];
    public $quotation;
    public $service_id = [];
    public function mount()
    {
        # code...
        // unset($this->service_id);
    }
    public function render()
    {
        $types = Type::orderBy('name', 'asc')->get();
        $events = $this->quotation->events;
        return view('livewire.admin.quotation.event.list-event', compact('events', 'types'));
    }
    public function incrementList()
    {
        dd('delete ok!!!');
    }
    public function delete(Event $event)
    {
        // return dd('Eliminando!!!');
        $event->delete();
        $this->emit('updateEvent');
    }
    public function addItem($id)
    {
        // return dd($id);
        // dd($this->service_id);
        $event = Event::find($id);

        $event->services()->syncWithoutDetaching($this->service_id);
        // session()->flash('info', 'Los items de servicios para el evento se han agregado con éxito.');
        $this->dispatchBrowserEvent('close-modal-item', ['id' => $id]);
        $this->emit('list-item-render');
        // unset($this->service_id);
        $this->service_id = [];
    }
    public function close($id)
    {
        // dd($id);
        unset($this->service_id);
        $this->dispatchBrowserEvent('close-modal-item', ['id' => $id]);
    }
}
