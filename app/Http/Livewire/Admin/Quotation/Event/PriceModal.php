<?php

namespace App\Http\Livewire\Admin\Quotation\Event;

use App\Models\Service;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class PriceModal extends Component
{
    protected $listeners = ['update'];
    protected $rules = [
        'qty' => 'required',
        'price' => 'required',
    ];
    public $eventserv_id;
    private $eventserv = [];
    private $service;
    // public $qty, $price;
    public $title, $unit;
    public function mount()
    {
        $this->eventserv = DB::table('event_service')->find($this->eventserv_id);
        $this->service = Service::find($this->eventserv->service_id);
        $this->qty = $this->eventserv->quantity;
        $this->title = $this->service->name;
        $this->unit = $this->service->unit;
        if ($this->eventserv->price <= 0) {
            $this->price = $this->service->price;
        } else {
            $this->price = $this->eventserv->price;
        }
    }
    public function render()
    {
        return view('livewire.admin.quotation.event.price-modal');
    }
    public function update()
    {
        $this->validate($this->rules);
        if ($this->qty <= 0) {
            return session()->flash('msgq', 'La cantidad no puede ser menor igual a cero!');
        } elseif ($this->price <= 0) {
            return session()->flash('msgp', 'El precio no puede ser menor igual a cero!');
        } else {
            # code...
            DB::table('event_service')->where('id', $this->eventserv_id)->update(['quantity' => $this->qty, 'price' => $this->price]);
            $this->dispatchBrowserEvent('price-updated', ['id' => $this->eventserv_id]);
            $this->emit('updatePrice');
            $this->emit('list-item-render');
        }
    }
}
