<?php

namespace App\Http\Livewire\Admin\Quotation\Service;

use App\Models\Type;
use Livewire\Component;
use Livewire\WithPagination;

class ListType extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['updateTypeList' => 'render', 'destroy'];
    public $search;
    public function render()
    {
        $types = Type::where('name', 'like', '%' . $this->search . '%')
            ->latest()
            ->paginate(8);
        // $types = Type::latest()->paginate(8);
        return view('livewire.admin.quotation.service.list-type', compact('types'));
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function destroy(Type $type)
    {
        $type->delete();
    }
}
