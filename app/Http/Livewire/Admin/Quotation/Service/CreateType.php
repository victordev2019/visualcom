<?php

namespace App\Http\Livewire\Admin\Quotation\Service;

use App\Models\Type;
use Livewire\Component;

class CreateType extends Component
{
    protected $rules = [
        'name' => 'required'
    ];
    public $name;
    public function render()
    {
        return view('livewire.admin.quotation.service.create-type');
    }
    public function store()
    {
        $this->validate($this->rules);
        Type::create([
            'name' => $this->name
        ]);
        $this->reset('name');
        $this->emit('updateTypeList');
        $this->dispatchBrowserEvent('success-dialog', ['msg' => 'El tipo de servicio se ha creado!']);
        $this->dispatchBrowserEvent('close-collapse-type');
    }
}
