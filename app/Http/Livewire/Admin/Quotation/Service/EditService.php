<?php

namespace App\Http\Livewire\Admin\Quotation\Service;

use Livewire\Component;
use App\Models\Service;

class EditService extends Component
{
    public $service;
    public $name, $unit, $price;
    protected $rules = [
        'name' => 'required',
        'unit' => 'required|max:2',
        'price' => 'required'
    ];
    public function mount()
    {
        $this->name = $this->service->name;
        $this->unit = $this->service->unit;
        $this->price = $this->service->price;
    }
    public function render()
    {
        $units = Service::distinct()->get(['unit']);
        return view('livewire.admin.quotation.service.edit-service', compact('units'));
    }
    public function edit()
    {
        $this->validate($this->rules);
        if ($this->price <= 0) {
            return session()->flash('msgp', 'El precio no puede ser menor igual a cero!');
        }
        $this->service->name = $this->name;
        $this->service->unit = $this->unit;
        $this->service->price = $this->price;
        $this->service->save();
        $this->service->refresh();
        $this->emitTo('admin.quotation.service.edit-type', 'render');
        $this->dispatchBrowserEvent('success-dialog', ['msg' => 'El servicio se ha modificado!']);
        $this->dispatchBrowserEvent('close-edit-service', ['id' => $this->service->id]);
    }
}
