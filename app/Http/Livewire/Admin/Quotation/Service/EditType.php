<?php

namespace App\Http\Livewire\Admin\Quotation\Service;

use App\Models\Service;
use App\Models\Type;
use Livewire\Component;

class EditType extends Component
{
    public $type;
    public $type_name;
    public $name, $unit, $price;
    // protected $listeners = ['fillServices' => 'render'];
    protected $listeners = ['render'];
    protected $rules = [
        'name' => 'required',
        'unit' => 'required|max:2',
        'price' => 'required',
    ];
    public function mount()
    {
        $this->type_name = $this->type->name;
    }
    public function render()
    {
        $units = Service::distinct()->get(['unit']);
        return view('livewire.admin.quotation.service.edit-type', compact('units'));
    }
    public function edit_type()
    {
        $this->validate([
            'type_name' => 'required'
        ]);
        $this->type->name = $this->type_name;
        $this->type->save();
        $this->emit('updateTypeList');
        // dd('edit ok!!!');
    }
    public function store_service()
    {
        // $rules = [
        //     'name' => 'required'
        // ];
        $this->validate($this->rules);
        if ($this->price <= 0) {
            return session()->flash('msgp', 'El precio no puede ser menor igual a cero!');
        }
        $this->type->services()->create([
            'name' => $this->name,
            'unit' => $this->unit,
            'price' => $this->price
        ]);
        $this->reset([
            'name',
            'unit',
            'price'
        ]);
        $this->type->refresh();
        session()->flash('msg', 'El servicio se ha creado con éxito!');
        $this->dispatchBrowserEvent('close-store-service', ['id' => $this->type->id]);
        // dd('add service ok!!!');
    }
    public function test()
    {
        $this->type->refresh();
    }
}
