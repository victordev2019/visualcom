<?php

namespace App\Http\Livewire\Admin\Quotation;

use App\Models\Client;
use Livewire\Component;

class EditQuotation extends Component
{
    protected $rules = [
        'date' => 'required',
        'reference' => 'required',
        'status' => 'required'
    ];
    public $quotation;
    public $date, $mind, $maxd, $reference, $status;
    public $clients, $client_id;
    public function mount($quotation)
    {
        // dd(Date('Y-m-d'));
        $this->date = $quotation->date;
        $this->code = $quotation->code;
        $this->reference = $quotation->reference;
        $this->client_id = $quotation->client_id;
        $this->status = $quotation->status;
        $this->mind = date('Y') . '-01-01';
        $this->maxd = date('Y') . '-12-31';
    }
    public function render()
    {
        $this->clients = Client::orderBy('name', 'asc')->get();
        return view('livewire.admin.quotation.edit-quotation');
    }
    public function save()
    {
        $this->validate($this->rules);
        $this->quotation->date = $this->date;
        $this->quotation->reference = strtoupper($this->reference);
        $this->quotation->status = $this->status;
        $this->quotation->save();
        return redirect()->route('quotations.index')->with('info', 'Los datos se han actualizado correctamente.');
    }
}
