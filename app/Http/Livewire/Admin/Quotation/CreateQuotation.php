<?php

namespace App\Http\Livewire\Admin\Quotation;

use App\Models\Client;
use App\Models\Quotation;
use App\Models\User;
use JeroenNoten\LaravelAdminLte\View\Components\Tool\Modal;
use Livewire\Component;

class CreateQuotation extends Component
{
    protected $rules = [
        'code' => 'required',
        'date' => 'required|date',
        'reference' => 'required',
        'client_id' => 'required'
    ];
    public $date, $mind, $maxd;
    public $client_id;
    public $code, $reference;
    // variables cliente
    public $name, $responsible, $email, $phone;
    // protected $rules_client = [
    //     'name' => 'required'
    // ];
    public function mount()
    {
        $this->date = date('Y-m-d');
        // $this->date = now();
        $this->mind = date('Y') . '-01-01';
        $this->maxd = date('Y') . '-12-31';
        $this->code = $this->code();
    }
    public function render()
    {
        $clients = Client::orderBy('name', 'asc')->get();
        return view('livewire.admin.quotation.create-quotation', compact('clients'));
    }
    public function store(User $user)
    {
        $this->validate($this->rules);
        $quotation = new Quotation();
        $quotation->code = $this->code;
        $quotation->date = $this->date;
        $quotation->reference = $this->reference;
        $quotation->user_id = $user->id;
        $quotation->client_id = $this->client_id;

        $quotation->save();

        $quo = Quotation::find($quotation->id);
        // $quo = Quotation::find(5);
        // $quo->last();
        // return dd($quo->id);
        return redirect()->route('event.create', $quo);
        // return redirect()->route('quotations.index');
    }
    public function storeClientData()
    {
        $this->validate([
            'name' => 'required',
            'responsible' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);
        $client = new Client();
        $client->name = $this->name;
        $client->responsible = $this->responsible;
        $client->email = $this->email;
        $client->phone = $this->phone;
        $client->save();
        $this->clearModal();
        session()->flash('info', 'Cliente agregado con éxito.');
        $this->dispatchBrowserEvent('close-modal');
    }
    public function clearModal()
    {
        $this->name = '';
        $this->responsible = '';
        $this->email = '';
        $this->phone = '';
    }
    // generar código
    public function code()
    {
        $n = 1;
        $quotations = Quotation::all();
        if ($quotations->count()) {
            $c = $quotations->last()->code;
            $y = substr($c, mb_strlen($c) - 4, mb_strlen($c));
            // $y = '2021';
            if ($y === date('Y')) {
                $n = intval(substr($c, 0, strpos($c, '-'))) + 1;
            } else {
                $n = 1;
            }

            $nc = $n . '-VCS-' . date('Y');
            return $nc;
        } else {
            return $n . '-VCS-' . date('Y');
        }
        // return date('d-m-Y');
    }
}
