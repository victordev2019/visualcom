<?php

namespace App\Http\Livewire\Admin\Quotation;

use App\Models\Client;
use App\Models\Quotation;
use App\Models\Service;
use Livewire\Component;

class ListQuotation extends Component
{
    protected $listeners = ['destroy'];
    public $search;
    public $sort = 'code';
    public $direction = 'desc';
    public function render()
    {
        $quotations = Quotation::with('client')
            ->where('code', 'like', '%' . $this->search . '%')
            // ->orWhere('reference', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->get();
        $clients = Client::all();
        return view('livewire.admin.quotation.list-quotation', compact('quotations'));
    }
    public function order($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
    public function destroy(Quotation $quotation)
    {
        // dd($quotation);
        $quotation->delete();
    }
}
