<?php

namespace App\Http\Livewire\Admin\Quotation;

use App\Models\Quotation;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Luecano\NumeroALetras\NumeroALetras;

class PaymentData extends Component
{
    protected $listeners = ['updatePrice' => 'render'];
    public $quotation;
    public $pricel;
    public $payment;
    public $price;

    public function mount()
    {
        $formater = new NumeroALetras();
        $this->pricel = $formater->toInvoice($this->quotation->price, 2, 'bolivianos');
    }
    public function render()
    {
        $this->payment = $this->quotation->payment;
        $this->price = 0;
        foreach ($this->quotation->events as $event) {
            $services = DB::table('event_service')->where('event_id', $event->id)->get();
            foreach ($services as $service) {
                # code...
                $this->price = $this->price + $service->quantity * $service->price;
            }
        }
        // return $this->quotation->price;
        $formater = new NumeroALetras();
        $this->pricel = $formater->toInvoice($this->price, 2, 'bolivianos');
        return view('livewire.admin.quotation.payment-data');
    }
    public function payment_data()
    {
        $this->quotation->payment = $this->payment;
        $this->quotation->save();
    }
    public function Price(Quotation $quotation, $price)
    {
        $quotation->price = $price;
        $quotation->save();
        $this->quotation->refresh();
        // dd($quotation);
        // return redirect()->route('event.create', $quotation->id);
    }
}
