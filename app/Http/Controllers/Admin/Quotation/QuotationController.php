<?php

namespace App\Http\Controllers\Admin\Quotation;

use App\Http\Controllers\Controller;
use App\Models\Quotation;
use Illuminate\Http\Request;
use App\Models\Event;

class QuotationController extends Controller
{
    public function index()
    {
        return view('admin.quotation.index');
    }
    public function create()
    {
        return view('admin.quotation.create');
    }
    public function eventCreate(Quotation $quotation)
    {
        return view('admin.quotation.event.index', compact('quotation'));
    }
    public function price(Request $request, $id)
    {
        $quo = Quotation::find($id);
        $quo->price = $request->price;
        $quo->save();
        return redirect()->route('event.create', $id);
        // return $request->price . '-' . $id;
    }
    public function edit(Quotation $quotation)
    {
        return view('admin.quotation.edit', compact('quotation'));
    }
}
