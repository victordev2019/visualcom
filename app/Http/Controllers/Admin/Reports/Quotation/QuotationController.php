<?php

namespace App\Http\Controllers\Admin\Reports\Quotation;

use App\Http\Controllers\Controller;
use App\Models\Quotation;
use App\Models\Event;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use Luecano\NumeroALetras\NumeroALetras;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    public $total = 0;
    public function index($id)
    {
        $quo = Quotation::find($id);
        $events = Event::where('quotation_id', $id)->get();
        $services = DB::table('event_service')
            ->select('event_service.id', 'event_service.quantity', 'services.name as service_name', 'services.type_id', 'types.name as type_name')
            ->join('services', 'services.id', '=', 'event_service.service_id')
            ->join('types', 'types.id', '=', 'services.type_id')
            ->orderBy('types.name')
            ->orderBy('services.name')
            ->where('event_service.event_id', $events->first()->id)->get();
        return view('reports.quotation', ['quo' => $quo, 'services' => $services, 'events' => $events]);
    }
    public function getQuotation($id)
    {
        $quo = Quotation::find($id);
        $events = Event::where('quotation_id', $id)
            ->orderBy('date', 'asc')
            ->get();
        // obtener total
        foreach ($events as $event) {
            $services = DB::table('event_service')->where('event_id', $event->id)->get();
            foreach ($services as $service) {
                $this->total = $this->total + $service->quantity * $service->price;
            }
        }
        $formater = new NumeroALetras();
        $price = $formater->toInvoice($this->total, 2, 'Bs');
        // $price = $formater->toInvoice($quo->price, 2, 'Bs');

        $pdf = Pdf::loadView('reports.quotation', ['quo' => $quo, 'events' => $events, 'price' => $price, 'total' => $this->total]);
        $pdf->set_paper(array(0, 0, 612.00, 792.00), 'portrait');
        $pdf->setOption(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return $pdf->stream($quo->code . '.pdf');
    }
}
