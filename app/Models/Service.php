<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    use HasFactory;
    // relations
    public function type()
    {
        return $this->belongsTo(Type::class);
    }
    public function events()
    {
        return $this->belongsToMany(Event::class);
    }
    public function eventService()
    {
        return $this->hasMany(EventService::class);
    }
}
