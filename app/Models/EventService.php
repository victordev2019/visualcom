<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventService extends Model
{
    use HasFactory;
    protected $table = 'event_service';
    // relations
    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
    public function types()
    {
        return $this->hasManyThrough(Type::class, Service::class, 'service_id', 'id');
    }
}
