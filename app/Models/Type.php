<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    // relations
    public function services()
    {
        return $this->hasMany(Service::class);
    }
    public function events()
    {
        return $this->hasManyThrough(Event::class, Service::class);
    }
}
