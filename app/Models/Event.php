<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    // relations
    public function quotation()
    {
        return $this->belongsTo(Quotation::class);
    }
    public function services()
    {
        return $this->belongsToMany(Service::class)->withPivot('quantity');
    }
    // public function services()
    // {
    //     return $this->belongsToMany(EventService::class, 'event_service');
    // }
}
