<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use HasFactory;
    const  ESPERA = 1, NEGOCIANDO = 2, ACEPTADO = 3, ARCHIVADO = 4, VENCIDO = 4;
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function getRouteKeyName()
    {
        return 'code';
    }
    // relations
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
